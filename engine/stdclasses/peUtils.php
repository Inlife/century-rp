<?php
/**
 * Description of peUtils
 *
 * @author Vladislav Gritsenko (Inlife)
 */

abstract class peUtils 
{
    public static function MailTo($Email, $Message) 
    {
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= "From: noreply <Admin>\r\n";
        mail($Email, crpEmail_Topic, $Message, $headers);
    }
    
    public static function Hash()
    {
        $Args = func_get_args();
        return md5(strrev(implode(peSys_HashSalt, $Args)));
    }
    
    public static function formUrl($Data, $Host = peSys_HostUrl)
    {
        if (!$Data) return $Host;
        return $Host . "?" . http_build_query($Data);
    }
}

?>