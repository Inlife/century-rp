<?php
/**
 * Description of peViewer
 *
 * @author Vladislav Gritsenko (Inlife)
 */
abstract class peViewer 
{
   protected static $Session;
    
    public static function getLanguage()
    {
        $Language = peCore::getInput($_COOKIE[peCookie_Lang]); 
        if (!$Language) 
            return peLang_Def;
        return $Language;  
    }
    
    public static function initSession()
    {
        session_start();
        self::$Session = &$_SESSION["_pe"];
    }
    
    public static function set($Name, $Value)
    {
        return self::$Session[$Name] = $Value; 		
    } 
    
    public static function get($Name)
    {
        if (isset(self::$Session[$Name]))
            return self::$Session[$Name]; 
        return false;   
    }
    
    public static function del()
    {
	$Names = func_get_args();
        foreach((array)$Names as $Name)
	{
	    $Data = self::$Session[$Name];
            unset(self::$Session[$Name]);
	}
	return @$Data;
    } 
    
    public static function destroySession()
    {
        session_destroy();
    }
}

?>
