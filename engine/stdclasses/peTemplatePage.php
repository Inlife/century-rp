<?php

/**
 * Description of peTemplatePage
 *
 * @author Vladislav Gritsenko (Inlife)
 */
abstract class peTemplatePage extends peTemplateBase
{
    public $Style;
    public $Language;
    
    public function __construct($Style, $FileName = peTpl_Page)
    {
        parent::__construct($Style . ds . $FileName);
        $this->Style = $Style;
        $this->loadLanguage( );
        $GLOBALS["CurrentPage"] = $this;
    }
    
    public function __destruct()
    {
        if (peSys_Status & peSys_Debug) $this->Debug = peCore::countTime();
        print $this->getContent();
    }
    
    public function getContent()
    {
        $Content = parent::getContent();
        // Constant replacement
        $Strings = array();
        preg_match_all(peTpl_ConstMask, $Content, $Strings);
        if ($Strings[1]) 
        {
        $Content = parent::getContent();
        // Constant replacement
        $Strings = array();
        preg_match_all(peTpl_ConstMask, $Content, $Strings);
        if ($Strings[1]) 
            $ConstBorders = explode("CONST", peTpl_ConstSyn);
            foreach($Strings[1] as $Const)
            $Content = str_replace($ConstBorders[0] . $Const . $ConstBorders[1], $this->getConstant($Const), $Content);
        }
        // Language replacement
        $LangBorders = explode("TEXT", peTpl_TextSyn);
        foreach($this->Language as $Name => $Code)
            $Content = str_replace($LangBorders[0] . $Name . $LangBorders[1],  $Code, $Content);
        return $this->linksFilter($Content);
    }
    
    protected function getConstant($Const)
    {
        return @constant($Const);
    }
    
    protected function loadLanguage($Language = peLang_Def)
    {
        if ($Data = peCore::file(peLang_Dir . $Language . peLang_Ext))
        {
            foreach($Data as $Str)
            {
                if (trim($Str)) 
                {
                    $Values = explode(peLang_Explode, $Str);
                    $this->Language[trim($Values[0])] = trim($Values[1]);
                }   
            }    
        }
    }
    
    protected function linksFilter($Data)
    {
        $Data = preg_replace(
            '#(href|src|action)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#',
            '$1="'. peSys_HostUrl .'$2$3',
            $Data);
        $Data = str_replace(
            peSys_HostUrl . peTpl_CssDir, 
            peSys_HostUrl . peTpl_Dir . $this->Style . ds . peTpl_CssDir, 
            $Data);
        return $Data;
    }
}

?>
