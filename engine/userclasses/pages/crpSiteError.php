<?php

/**
 * Description of crpPage
 *
 * @author Vladislav Gritsenko (Inlife)
 */

class crpSiteError extends crpSitePage
{
    public function __construct()
    {
        parent::__construct("{(error.title)}", "message.back");
        $this->Content->Title = "{(error.title)}";
        $this->Scripts = new peJScript("history.back");
    }
}

?>
