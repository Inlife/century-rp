<?php

/*
 *  @Author: Vladislav Gritsenko (Inlife)
 *  @Name: articles
 *  @Project: Proto Engine CMS
 */
require("../core.php");

$Page = new crpAdminArticles();

@$Action = peCore::getInput($_GET["action"]);
switch($Action)
{
    default:
	
	
	@$Msg = peCore::getInput($_GET["msg"]);
	if ($Msg)
	{
	    $Page->Content->Message = new peBlock("message");
	    $Page->Content->Message->Text = '{('.$Msg.')}';
	}
	@$Code = peCore::getInput($_GET["page"]);
	if (!$Code or $Code < 1) $Code = 0;
	
	$News = crpDB::getNews($Code * crpNews_AdminPerPage, crpNews_AdminPerPage);
	foreach($News as $i => $Article)
	{
	    $Blocks[$i] = new peBlock("article");
	    $Blocks[$i]->Title = $Article->Title;
	    $Blocks[$i]->Author = $Article->Author;
	    $Blocks[$i]->Date = $Article->Date;
	    $Blocks[$i]->EditUrl = crpUtils::formUrl(array("action" => "edit", "id" => $Article->ID), crpPage_AdminArticles);  
	    $Blocks[$i]->DeleteUrl = crpUtils::formUrl(array("action" => "delete", "id" => $Article->ID), crpPage_AdminArticles); 
	}
	$Page->Content->Articles = new peComponents($Blocks);
	break;
	
	
    case "add":

	
	$Response = peCore::getResponse("title", "forum", "content:h", "author", "id:i");
	$Params = array(
	    "Title" => $Response->Title,
	    "Forum" => $Response->Forum,
	    "Content" => $Response->Content,
	    "Author" => $Response->Author
	);
	if ($Response->ID) 
	{
	    crpDB::updArticle($Params, array("ID" => $Response->ID));
	    crpAdminArticles::redirectThere(array("msg" => "admin.article.update.success"));
	} else { 
	    crpDB::addArticle($Params);
	    crpAdminArticles::redirectThere(array("msg" => "admin.article.add.success"));
	}
	break;

	
    case "delete":
	
	
	@$ID = peCore::getInput($_GET["id"]);
	if ($ID) 
	{
	    crpDB::delArticle(array("ID" => $ID));
	    crpAdminArticles::redirectThere(array("msg" => "admin.article.delete.success"));
	}
	break;

	
    case "edit":

	
	@$ID = peCore::getInput($_GET["id"]);
	$Page->Content = new peContent("articles.form");
	$Page->Content->FormLink = crpUtils::formUrl(array("action" => "add"), crpPage_AdminArticles);
	$Page->Content->Author = crpUser::getData()->Login;
	if ($ID) 
	{
	    $Article = crpDB::getArticle(array("ID" => $ID));
	    $Page->Content->Title = $Article->Title;
	    $Page->Content->ID = $Article->ID;
	    $Page->Content->Forum = $Article->Forum;
	    $Page->Content->Author = $Article->Author;
	    $Page->Content->Content = $Article->Content;
	}
	break;
}
?>
