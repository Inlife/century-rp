<?php

/*
 * Main Index File
 * @Proto CMS
 */
require('core.php');

$Page = new crpSiteError();
 
@$Code = peCore::getInput($_GET["code"], peInput_Int); if (empty($Code)) $Code = 0;
@$Step = peCore::getInput($_GET["step"], peInput_Int); if (empty($Step)) $Step = 1;

switch($Code)
{
    default: 	$Error = "{(error.unkown)}";               break;
    case 1: 	$Error = "{(error.emptyfields)}";          break;
    case 2: 	$Error = "{(error.loginformat)}";          break;
    case 3: 	$Error = "{(error.passwordsdifferent)}";   break;
    case 4:	$Error = "{(error.emailformat)}";          break;
    case 5:	$Error = "{(error.testerror)}";            break;
    case 6:	$Error = "{(error.userwithlogormail)}";    break;
    case 7:	$Error = "{(error.loginlenth)}";           break;
    case 8:	$Error = "{(error.passwordlenth)}";        break;
    case 9:	$Error = "{(error.regtimepassed)}";        break;
    case 10:	$Error = "{(error.nosuchuser)}";           break;
    case 11:	$Error = "{(error.alreadyauthed)}";        break;
    case 12:	$Error = "{(error.notlogined)}";           break;
    case 13:	$Error = "{(error.testnotpassed)}";        break;
    case 14:	$Error = "{(error.stringformat)}";         break;
    case 15:	$Error = "{(error.stringlenth)}";          break;
    case 16:	$Error = "{(error.charlimit)}";            break;
    case 17:	$Error = "{(error.charalreadycreated)}";   break;
    case 18:	$Error = "{(error.usernothaveright)}";     break;
    case 19:	$Error = "{(error.user.activeorfalse)}";   break;
    case 20:    $Error = "{(error.character.notexists)}";  break;
    case 21:    $Error = "{(error.article.notexists)}";	   break;
    case 22:    $Error = "{(error.character.activated)}";  break;
    case 23:    $Error = "{(error.register.captcha)}";	   break;
    case 24:    $Error = "{(error.character.age)}";	   break;

    case 403:	$Error = "{(error.usernothaveright)}";     break;
    case 404:	$Error = "{(error.404)}";                  break;
    case 500:	$Error = "{(error.500)}";                  break;
}   
$Page->Scripts->Step = $Step;
$Page->Content->Text = $Error;
?>