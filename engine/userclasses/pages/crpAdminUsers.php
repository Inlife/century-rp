<?php

/**
 * Description of crpPage
 *
 * @author Vladislav Gritsenko (Inlife)
 */

class crpAdminUsers extends crpAdminPage
{
    
    public function __construct()
    {
        parent::__construct("{(admin.title)}", "users");
	$this->requestPermission(crpPerm_UsersMin);
    }
}

?>
