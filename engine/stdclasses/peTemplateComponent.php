<?php

/**
 * Description of peTemplatePage
 *
 * @author Vladislav Gritsenko (Inlife)
 */

abstract class peTemplateComponent extends peTemplateBase
{
    public function __construct($FileName, $Ext = null)
    {
        parent::__construct($FileName, $Ext);
    }
    
    public function setParent($Parent)
    {
        $this->FileName = $Parent->getStyle() . ds . $this->FileName;
    }
    
    public function getContent()
    {
        return parent::getContent();
    }
}

?>
