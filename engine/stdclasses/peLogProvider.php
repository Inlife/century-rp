<?php

/**
 * Description of peLogProvider
 *
 * @author Vladislav Gritsenko (Inlife)
 */

class peLogProvider
{
    protected static function getUserLogData()
    {
	return array(
		self::getTime(),
		$_SERVER["REMOTE_ADDR"],
		$_SERVER["PHP_SELF"], 
		$_SERVER["REQUEST_METHOD"],
		$_SERVER["QUERY_STRING"],
	    );
    }
    
    protected static function getTime()
    {
	return date("Y-m-d H:i:s");
    }
    
    public static function LogCall($File)
    {
	
	$Params = self::getUserLogData();
	if ($User = crpUser::getData())
	{
	    $Params[] = $User->Login;
	    $Params[] = $User->Rank;
	} else {
	    $Params[] = null;
	    $Params[] = null;
	}
	$String = vsprintf("[%s] - IP: %s, Script: %s, Method: %s, Query: %s, User: %s, Rank: %s\n", $Params);
	peCore::fileSet($File, $String, FILE_APPEND);	
    }
    
    public static function EmailLogCall()
    {
	$String = "[".self::getTime()."] - " .  implode("|", func_get_args()) . "\n";
	peCore::fileSet(peLog_EmailFile, $String, FILE_APPEND);
    }
    
    public static function ModuleLogCall()
    {
	$Params = self::getUserLogData();
	if (crpUser::getData())
	{
	    $Params[] = crpUser::getData()->Login;
	    $Params[] = crpUser::getData()->Rank;
	} else {
	    $Params[] = null;
	    $Params[] = null;
	}
	$Params[] = implode("|", func_get_args());
	$String = vsprintf("[%s] - IP: %s, Script: %s, Method: %s, Query: %s, User: %s, Rank: %s, Params: %s\n", $Params);
	peCore::fileSet(peLog_ModuleFile, $String, FILE_APPEND);	
    }
}

?>
