<?php
/*
 *  @Author: Vladislav Gritsenko (Inlife)
 *  @Name: settings
 *  @Project: Proto Engine CMS
 */
// Getting current url-dir
$_Str = explode("/", $_SERVER["REQUEST_URI"]); array_pop($_Str);

// Dirs
define("ds", "/");
define("peEngine_Dir", "engine" . ds);
define("peData_Dir", "data" . ds);

// Logs
define("peLog_Dir", "logs" . ds);
define("peLog_CharFile", peLog_Dir . "ucp.character.sadd.log");
define("peLog_EmailFile", peLog_Dir. "email.log");
define("peLog_UserLoginFile", peLog_Dir . "user.logins.log");
define("peLog_UserRegisterFile", peLog_Dir . "user.register.log");
define("peLog_AdminPanelFile", peLog_Dir . "adminpanel.log");
define("peLog_ModuleFile", peLog_Dir . "module.log");

// Input-Output Checking
define("peIO_InCharset", "utf-8");
define("peIO_OutCharset", "utf-8");
define("peIO_FileErrorMsg", "<b>Error</b>: File \"%s\" does not exists");

// Languages
define("peLang_Dir", "lang" . ds);
define("peLang_Ext", ".lang");
define("peLang_Def", "ru");
define("peLang_Explode", "=");

define("peIO_Trim", 1);
define("peIO_Spec", 2);
define("peIO_Slash", 4);
define("peIO_Substr", 8);
define("peIO_Int", 16);
define("peIO_Bool", 32);
define("peIO_NltoBr", 64);

define("peInput_Html", peIO_Trim    | peIO_NltoBr);
define("peInput_Text", peInput_Html | peIO_Spec | peIO_Slash);
define("peInput_Str",  peInput_Text | peIO_Substr);
define("peInput_Int",  peInput_Str  | peIO_Int);
define("peInput_Bool", peInput_Str  | peIO_Bool);

// System constants
define("peSys_IncludeFileExt", ".php");
define("peSys_CoreVersion", "3.0.0");
define("peSys_Author", "Inlife");
define("peSys_SecCode", "32whs64");
define("peSys_Debug", 1);
define("peSys_Release", 2);
define("peSys_Status", peSys_Debug);
define("peSys_HashSalt", "qw123");
define("peSys_HostFile", "host");
define("peSys_CurrentUrl", "http://" . $_SERVER["HTTP_HOST"] . implode("/", $_Str) . "/");
define("peSys_HostUrl", (($GLOBALS["_host"] = peCore::fileGet(peSys_HostFile)) == true ? $GLOBALS["_host"]: peSys_CurrentUrl));

// Template System
define("peTpl_VarMask", '/\{\$([A-Z0-9_\.]{1,32})\$\}/');
define("peTpl_VarSyn", '{$VAR$}');
define("peTpl_ConstMask", '/\{\%([a-zA-Z0-9_]{1,32})\%\}/');
define("peTpl_ConstSyn", '{%CONST%}');
define("peTpl_TextSyn", '{(TEXT)}');
define("peTpl_Ext", ".html");
define("peTpl_Dir", "tpl" . ds);
define("peTpl_ComponentsDir", "components" . ds);
define("peTpl_Page", "index");
define("peTpl_ContentPrefix", "content.");
define("peTpl_BlockPrefix", "block.");
define("peTpl_CssDir", "css");

// ProtoEngine JavaScript dynamic-include scripts (peJS)
define("peJS_Ext", ".pejs");
define("peJS_Dir", "scripts" . ds);

// Cookies
define("peCookie_Lang", "language");
define('peCookie_Expire', time() + 60 * 60 * 24 * 30);

// Other
define("peData_EmailExt", ".txt");
define("NL", "\n");
define("RL", "\r");
define("_BR_", RL . NL);

?>
