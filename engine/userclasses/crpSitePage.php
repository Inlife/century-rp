<?php

/**
 * Description of crpSitePage
 *
 * @author Vladislav Gritsenko (Inlife)
 */
class crpSitePage extends pePage 
{
    public function __construct($Title, $Content, $Style = crpStyle_Site) 
    {
        parent::__construct($Style);
        $this->Title = crpSite_TitlePrefix . $Title;
        if ($Content)	$this->Content = new peContent($Content);
	$this->Login = "{(ucp.guest)}";
        $this->ServerInfo();
        $this->UCPMenu = new peBlock("sidebar.login");
        if (crpUser::isLogined())
        {
            $this->UCPMenu = new peBlock("sidebar.control");
	    $this->Login = crpUser::getData()->Login;
            $this->UCPMenu->Logout_Link = crpUtils::formUrl(array("action" => "logout"), crpPage_SiteLogin);
	    if (crpUser::getData()->Rank >= crpPerm_Adminpage)
		$this->AdminUrl = crpPage_AdminIndex;
        }
	peCore::fileSet(peSys_HostFile, peSys_CurrentUrl);
	$this->LoadWigets();
    }
    
    public function ServerInfo()
    {
        $this->UcpVersion = "1.1.5 Beta";
        $this->ModVersion = "0.2.405";
        $this->Serverstatus = "<span style='color: red'>{(server.status.offline)}</span>";
        $this->Players = "0 / 0";
        $this->Samp = new SampQueryAPI("46.28.68.119");
        if ($this->Samp->isOnline()) {
            $this->Serverstatus = "<span style='color: green'>{(server.status.online)}</span>";
            $Data = (Object)$this->Samp->getInfo();
            $this->Players = $Data->players . " / " . $Data->maxplayers;
         }
    }
    
    public function LoadWigets()
    {
	$Blocks[1] = new peBlock("wiget");
	$Blocks[1]->Title = "{(site.partners.title)}";
	$Blocks[1]->Text = peCore::fileGet(peData_Dir . "wigets.partners.txt");
	$Blocks[2] = new peBlock("wiget");
	$Blocks[2]->Title = "{(site.advert.title)}";
	$Blocks[2]->Text = peCore::fileGet(peData_Dir . "wigets.advert.txt");
	$this->Wigets = new peComponents($Blocks);
    }
    
    public function addToTitle($String)
    {
	$this->Title = crpSite_TitlePrefix . $String;
    }
}

?>
