<?php

/*
 *  @Author: Vladislav Gritsenko (Inlife)
 *  @Name: ucp
 *  @Project: Proto Engine CMS
 */
require("core.php");

$Page = new crpSiteUcp();

if (!crpUser::isLogined()) peCore::throwError(11);

@$Action = peCore::getInput($_GET['action']);

switch ($Action) {
    case "characters":
	
	
	$Page->Title = crpSite_TitlePrefix . "{(ucp.characters)}";
	$Page->Content = new peContent("ucp.characters");
	
	$User = crpUser::getData();
	$Characters = crpDB::getCharacters(array("Account" => $User->Login));
	
	foreach($Characters as $i => $Character) 
	{
	    $Result[$i] = new peBlock("ucp.characters.list");
	    $Result[$i]->Name = $Character->Name;
	    $Result[$i]->Skin = $Character->Skin;
	    $Result[$i]->Age = $Character->Age;
	    $Result[$i]->Sex = ($Character->Sex == 1 ? "{(ucp.characters.sex.m)}" : "{(ucp.characters.sex.w)}");
	    switch($Character->Activated)
	    {
		case 0: $Result[$i]->Active = "{(ucp.characters.active.false)}"; break;
		case 1: $Result[$i]->Active = "{(ucp.characters.active.true)}"; break;
		case 2: $Result[$i]->Active = "{(ucp.characters.active.declined)}"; break;
	    }
	}
	if (isset($Result))
	    $Page->Content->UcpCharlist = new peComponents($Result);

	if (@count($Result) <= crpUtils::getMaxCharactersBy($User->Rank)) 
	{
	    $Page->Content->UcpAddCharLink = new peBlock("ucp.characters.add.link");
	    $Page->Content->UcpAddCharLink->Link = crpUtils::formUrl(array("action" => "characters.add"), crpPage_SiteUcp);
	}
	break;
	
	
    case "characters.add":
	
	
	$Page->addJS("ucp.skin.change");
	$Page->Content = new peContent("ucp.characters.add");
	$Page->Content->FormLink = crpUtils::formUrl(array("action" => "characters.add.process"), crpPage_SiteUcp);
	for ($i = 0; $i < 4; $i++) 
	{
	    $Questions[$i] = new peBlock("ucp.characters.add.question");
	    $Questions[$i]->Title = "{(ucp.characters.question.$i)}";
	    $Questions[$i]->ID = $i;
	}
	$Page->Content->Questions = new peComponents($Questions);
	break;
	
	
    case "characters.add.process":
	
	
	// logs
	peLogProvider::LogCall(peLog_CharFile);
	$Response = peCore::getResponse("firstname", "lastname", "password", "repassword", "age:i", "sex:i", "skin:i");

	for ($i = 0; $i < 4; $i++)
	    $Questions[$i] = peCore::getInput($_POST["textquestion" . $i], peInput_Text);

	$Response->Answers = implode(crpUser_CharImplode, $Questions);
	
	if (!$Response->Sex)
	    peCore::throwError(1);
	if (!$Response->Skin)
	    $Response->Skin = 0;
	if ($Response->Password != $Response->Repassword)
	    peCore::throwError(3);
	if (!$Response->Age)
	    peCore::throwError(1);
	if($Response->Age < crpChar_MinAge or $Response->Age > crpChar_MaxAge)
	    peCore::throwError(24);
	
	$User = crpUser::getData();
	$Response->Name = crpUtils::Name($Response->Firstname) . "_" . crpUtils::Name($Response->Lastname);
	$Response->Account = $User->Login;
	
	$Characters = crpDB::getCharacters(array("Account" => $User->Login));
	if (count($Characters) >= CRPUtils::getMaxCharactersBy($User->Rank))
	   peCore::throwError(16);

	$Result = crpDB::getCharacters(array("Name" => $Response->Name)); 
	if (count($Result) > 0) 
	    peCore::throwError(17);
	
	$Params = array(
            "Name" => $Response->Name,
            "Password" => strtoupper(crpUtils::Password($Response->Password)),
	    "Skin" => $Response->Skin,
	    "Sex" => $Response->Sex,
	    "Answers" => $Response->Answers,
	    "Account" => $Response->Account,
	    "Age" => $Response->Age
        );
	crpDB::addCharacter($Params);
	crpUtils::Mail("email.ucp.character.added", $User->Email, $Response->Name, $Response->Password);

	$Page->Content = new peContent("message.page");
	$Page->Content->Title = "{(ucp.characters.added.title)}";
	$Page->Content->Text = "{(ucp.characters.added.text)}";
	$Page->Content->Link = crpUtils::formUrl(array("action" => "characters"), crpPage_SiteUcp);
	break;
	
	
}
?>
