<?php

/**
 * Description of crpPage
 *
 * @author Vladislav Gritsenko (Inlife)
 */

class crpAdminArticles extends crpAdminPage
{
    
    public function __construct()
    {
        parent::__construct("{(admin.title)}", "articles");
	$this->requestPermission(crpPerm_ArticlesMain);
    }
}

?>
