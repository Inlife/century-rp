<?php

/*
 *  @Author: Vladislav Gritsenko (Inlife)
 *  @Name: characters
 *  @Project: Proto Engine CMS
 */

require("../core.php");

$Page = new crpAdminCharacters();

@$Action = peCore::getInput($_GET["action"]);
switch($Action)
{
    default:
	
	
	$Page->requestPermission(crpPerm_CharactersMain);
	@$Code = peCore::getInput($_GET["page"]);
	if (!$Code or $Code < 1) $Code = 0;
	
	$Characters = crpDB::getPlayers($Code * crpNews_AdminPerPage, crpNews_AdminPerPage);
	foreach($Characters as $i => $Character)
	{
	    $Blocks[$i] = new peBlock("character");
	    $Blocks[$i]->Name = $Character->Name;
	    $Blocks[$i]->Account = new peBlock("link");
	    $Blocks[$i]->Account->Text = $Character->Account;
	    $Blocks[$i]->Account->Link = crpUtils::formUrl(array("action" => "details", "name" => $Character->Account), crpPage_AdminUsers);
	    switch($Character->Activated)
	    {
		case 0: $Blocks[$i]->Activated = "{(ucp.characters.active.false)}"; break;
		case 1:	$Blocks[$i]->Activated = "{(ucp.characters.active.true)}"; break;
		case 2: $Blocks[$i]->Activated = "{(ucp.characters.active.declined)}"; break;
	    }
	    $Blocks[$i]->Date = $Character->Date;
	    $Blocks[$i]->EditUrl = crpUtils::formUrl(array("action" => "edit", "id" => $Character->ID), crpPage_AdminCharacters);  
	    $Blocks[$i]->DeleteUrl = crpUtils::formUrl(array("action" => "delete", "id" => $Character->ID), crpPage_AdminCharacters); 
	}
	@$Page->Content->Characters = new peComponents($Blocks);
	break;
	
	
    case "add":

	
	$Page->requestPermission(crpPerm_CharactersAdd);
	$Response = peCore::getResponse("name", "account", "skin:i", "sex:i", "age:i", "id:i", "activated:i");
	for($i = 0; $i < 4; $i++) $Answers[] = peCore::getInput($_POST["answer".$i], peInput_Html);
	$Params = array(
	    "Name" => $Response->Name,
	    "Account" => $Response->Account,
	    "Skin" => $Response->Skin,
	    "Sex" => $Response->Sex,
	    "Activated" => $Response->Activated,
	    "Age" => $Response->Age,
	    "Answers" => implode(crpUser_CharImplode, $Answers)
	);
	$User = crpDB::getUser(array("Login" => $Response->Account));
	if ($Response->ID) 
	{
	    crpDB::updCharacter($Params, array("ID" => $Response->ID));
	    crpAdminCharacters::redirectThere(array("msg" => "admin.characters.update.success"));
	} else { 
	    if (crpDB::getCharacter(array("Name" => $Response->Name))->ID) peCore::throwError(17);
	    $Password = substr(crpUtils::Hash(time(), rand()), 5, 8);
	    $Params["Password"] = strtoupper(md5($Password));
	    crpDB::addCharacter($Params);
	    crpUtils::Mail("email.ucp.character.added", $User->Email, $Response->Name, $Password);
	    crpAdminCharacters::redirectThere(array("msg" => "admin.characters.add.success"));
	}
	break;

	
    case "delete":
	
	
	$Page->requestPermission(crpPerm_CharactersDelete);
	@$ID = peCore::getInput($_GET["id"]);
	if ($ID) 
	{
	    $Deleted = crpDB::delCharacter(array("ID" => $ID));
	    $User = crpDB::getUser(array("Login" => $Deleted->Account));
	    crpUtils::Mail("email.ucp.character.deleted", $User->Email, $Deleted->Name);
	    crpAdminCharacters::redirectThere(array("msg" => "admin.characters.delete.success"));
	}
	break;

	
    case "edit":

	
	$Page->requestPermission(crpPerm_CharactersEdit);
	@$ID = peCore::getInput($_GET["id"]);
	$Page->Content = new peContent("characters.form");
	$Page->Content->FormLink = crpUtils::formUrl(array("action" => "add"), crpPage_AdminCharacters);
	if ($ID) 
	{
	    $Character = crpDB::getCharacter(array("ID" => $ID));
	    if (!$Character->ID) peCore::throwError(20);
	    $Page->Content->Name = $Character->Name;
	    $Page->Content->ID = $Character->ID;
	    $Page->Content->Account = $Character->Account;
	    $Page->Content->Skin = $Character->Skin;
	    $Page->Content->Age = $Character->Age;
	    $Names = array(
		"Sex" . $Character->Sex,
		"Activated" . $Character->Activated
	    );
	    foreach($Names as $Name) $Page->Content->$Name = "selected";
	    $Answers = explode(crpUser_CharImplode, $Character->Answers);
	    foreach($Answers as $i => $Answer) 
	    {
		$Name = "answer" . $i;
		$Page->Content->$Name = $Answer;
	    }
	}
	break;
	

    case "details":
	
	
	$Page->requestPermission(crpPerm_CharactersActivate);
	@$ID = peCore::getInput($_GET["id"]);
	$Page->Content = new peContent("characters.activate");
	if ($ID) 
	{
	    $Page->Scripts = new peJScript("activate.form");
	    $Character = crpDB::getCharacter(array("ID" => $ID));
	    if (!$Character->ID) peCore::throwError(20);
	    $Page->Content->Name = $Character->Name;
	    $Page->Content->ID = $Character->ID;
	    $Page->Content->Age = $Character->Age;
	    $Page->Content->Account = new peBlock("link");
	    $Page->Content->Account->Text = $Character->Account;
	    $Page->Content->Account->Link = crpUtils::formUrl(array("action" => "details", "name" => $Character->Account), crpPage_AdminUsers);
	    $Page->Content->Skin = $Character->Skin;
	    $Page->Content->Sex = ($Character->Sex == 1 ? "{(ucp.characters.sex.m)}" : "{(ucp.characters.sex.w)}"); 
	    switch($Character->Activated)
	    {
		case 0: $Page->Content->Active = "{(ucp.characters.active.false)}"; break;
		case 1:	$Page->Content->Active = "{(ucp.characters.active.true)}"; break;
		case 2: $Page->Content->Active = "{(ucp.characters.active.declined)}"; break;
	    }
	    $Answers = explode(crpUser_CharImplode, $Character->Answers);
	    foreach($Answers as $i => $Answer) 
	    {
		$Name = "answer" . $i;
		$Page->Content->$Name = $Answer;
	    }
	    $Page->Content->LinkActivate = crpUtils::formUrl(array("action" => "update", "id" => $Character->ID, "active" => 1), crpPage_AdminCharacters);
	    $Page->Content->CharDeleteForm = crpUtils::formUrl(array("action" => "update", "id" => $Character->ID, "active" => 2), crpPage_AdminCharacters);
	}
	break;
	
	
    case "update":
	
	
	$Page->requestPermission(crpPerm_CharactersActivate);
	@$ID = peCore::getInput($_GET["id"]);
	@$Active = peCore::getInput($_GET["active"]);
	@$Message = peCore::getInput($_POST["message"]);
	if ($ID && $Active) 
	{
	    $Character = crpDB::getCharacter(array("ID" => $ID));
	    if (!$Character->ID) peCore::throwError(20);
	    if ($Character->Activated != 0) peCore::throwError(22);
	    $User = crpDB::getUser(array("Login" => $Character->Account));
	    crpDB::updCharacter(array("Activated" => $Active), array("ID" => $ID));
	    if (!$Message) $Message = "N/A";
	    if ($Active == 1)
		crpUtils::Mail("email.ucp.character.proved", $User->Email, $Character->Name);
	    elseif($Active == 2)
		crpUtils::Mail("email.ucp.character.disproved", $User->Email, $Character->Name, $Message);
		
	    crpAdminCharacters::redirectThere(array("action" => "activation", "msg" => "admin.characters.update.success"));
	}
	break;
	
    case "activation":
	
	
	$Page->requestPermission(crpPerm_CharactersActivate);
	$Characters = crpDB::getCharacters(array("Activated" => "0"));
	foreach($Characters as $i => $Character)
	{
	    $Blocks[$i] = new peBlock("character.activate");
	    $Blocks[$i]->Name = $Character->Name;
	    $Blocks[$i]->Account = $Character->Account;
	    switch($Character->Activated)
	    {
		case 0: $Blocks[$i]->Activated = "{(ucp.characters.active.false)}"; break;
		case 1:	$Blocks[$i]->Activated = "{(ucp.characters.active.true)}"; break;
		case 2: $Blocks[$i]->Activated = "{(ucp.characters.active.declined)}"; break;
	    }
	    $Blocks[$i]->Date = $Character->Date;
	    $Blocks[$i]->ActivateUrl = crpUtils::formUrl(array("action" => "details", "id" => $Character->ID), crpPage_AdminCharacters);  
	}
	@$Page->Content->Characters = new peComponents($Blocks);
	break;
	
}

?>
