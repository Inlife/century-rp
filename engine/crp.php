<?php

/*
 *  @Author: Vladislav Gritsenko (Inlife)
 *  @Name: crp
 *  @Project: Proto Engine CMS
 */

// Project Settings
define("crpSec_reCaptchaPublic", "6LdA09USAAAAAHmTUPImL0bW4O08_oXaJcKPWbgT");
define("crpSec_reCaptchaPrivate", "6LdA09USAAAAAOZ1w2PVJ3Q7cCAWhw9nBu5kmkS3");
define("crpStyle_Site", "site");
define("crpStyle_Admin", "admin");
define("crpSite_TitlePrefix", "Century RolePlay | ");
define("crpAdmin_TitlePrefix", "CRP Admin | ");
define("crpEmail_Topic", "Century RolePlay");

// Pages
define("crpPage_SiteIndex", peSys_HostUrl . "index.php");
define("crpPage_SiteError", peSys_HostUrl . "error.php");
define("crpPage_SiteLogin", peSys_HostUrl . "login.php");
define("crpPage_SiteRegister", peSys_HostUrl . "register.php");
define("crpPage_SiteUcp", peSys_HostUrl . "ucp.php");
define("crpPage_SitePage", peSys_HostUrl . "page.php");

define("crpPage_AdminDir", "admin" . ds);
define("crpPage_AdminIndex", peSys_HostUrl . crpPage_AdminDir . "index.php");
define("crpPage_AdminArticles", peSys_HostUrl . crpPage_AdminDir . "articles.php");
define("crpPage_AdminUsers", peSys_HostUrl . crpPage_AdminDir . "users.php");
define("crpPage_AdminCharacters", peSys_HostUrl . crpPage_AdminDir . "characters.php");

// News
define("crpNews_PerPage", 5);
define("crpNews_AdminPerPage", 30);

// Database
define("crpDB_Server", "46.28.68.119");
define("crpDB_User", "server");
define("crpDB_Password", "BJXhGV4vWyMCCKyS");
define("crpDB_Name", "crp");

define("crpDB_Articles", "articles");
define("crpDB_Accounts", "accounts");
define("crpDB_Characters", "playeraccounts");

define("crpDB_CharactersFields", "playerID,playerName,playerPassword,playerSkin,playerGender,playerActivated,playerRegistrationDate,playerAnswers,playerAccount,playerAge");

// Data
define("crpData_QuestionsSelect", "questions.select.txt");
define("crpData_QuestionsSelectLimit", 10);

// Users
define("crpAdmin_MinArticleRank", 4);

define("crpUser_LenghMinName", 3);
define("crpUser_LenghMin", 5);
define("crpUser_LenghMax", 32);
define("crpUser_RegTimeout", 60 * 60 * 3); // seconds * minutes * hours

define("crpUser_MaleSkins", "1,2,3,4,5,6,7,8,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,32,33,34,35,36,37,42,43,44,46,47,48,49,57,58,59,62,66,67,68,72,73,78,79,82,83,84,94,95,96,98,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,132,133,134,135,136,137,142,143,147,153,156,158,159,160,161,162,168,170,171,173,174,175,176,177,179,180,181,182,183,184,184,185,186,187,188,189,200,202,206,208,210,212,213,217,220,221,222,223,227,228,229,230,234,235,236,239,240,241,242,247,248,249,250,254,258,259,261,262,269,270,271,272,273,289,290,291,292,293,296,297,299");
define("crpUser_FemaleSkins", "9,10,11,12,13,31,38,39,40,41,53,54,55,56,63,64,65,69,75,76,77,85,88,89,90,91,93,129,130,131,141,148,150,151,152,157,169,172,190,191,192,193,194,195,196,197,198,199,201,207,211,214,215,216,218,219,224,225,226,231,232,233,237,238,243,245,263,298");

define("crpUser_CharImplode", "[#]");

define("crpUser_CharactersRank0", 3);
define("crpUser_CharactersRank1", 5);
define("crpUser_CharactersRank2", 7);
define("crpUser_CharactersRank3", 9);
define("crpUser_CharactersRank4", 11);
define("crpUser_CharactersRank5", 15);
define("crpUser_CharactersRank6", 100);

// Characters
define("crpChar_MinAge", 16);
define("crpChar_MaxAge", 100);

// Permissions
define("crpPerm_Adminpage", 3);

define("crpPerm_ArticlesMain", crpPerm_Adminpage + 1);

define("crpPerm_CharactersMain", crpPerm_Adminpage + 1);
define("crpPerm_CharactersAdd", crpPerm_CharactersMain);
define("crpPerm_CharactersEdit", crpPerm_CharactersAdd);
define("crpPerm_CharactersDelete", crpPerm_CharactersEdit);
define("crpPerm_CharactersActivate", crpPerm_Adminpage);

define("crpPerm_UsersMin", crpPerm_Adminpage + 1);

// Template Constants
define("YEAR", date("Y"));
?>