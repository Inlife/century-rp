<?php

/**
 * Description of crpPage
 *
 * @author Vladislav Gritsenko (Inlife)
 */
abstract class pePage extends peTemplatePage
{
    public function __construct($Style)
    {
        parent::__construct($Style);
    }
    
    public function Redirect($URL)
    {
        peCore::redirect($URL);
    }
    
    public function addJS()
    {
        $Args = func_get_args();
        foreach($Args as $peJScript)
            $Scripts[] = new peJScript($peJScript);
        $this->Scripts = new peComponents($Scripts);
    }
    
    public static function redirectThere($Params = null)
    {
        peCore::redirect(peUtils::formUrl(
            $Params, 
            constant("crpPage_" . self::getClassName()
        )));
    }
    
    public static function getClassName()
    {
        return str_replace("crp", "", get_called_class());
    }
}

?>
