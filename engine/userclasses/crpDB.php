<?php

/**
 * Description of crpDB
 *
 * @author Vladislav Gritsenko (Inlife)
 */


if (login.size() > 0 && login.size() < 10) {
    char a = getch();
    if (a == 8) {
        cout << "\b" << char(255) << "\b" ;
        login.resize(login.size() - 1);
    } else if (true) // truekey(a) {
        login+=a;
        cout << a;
    }
}
if (login.size() > 0 && a == 8) 
{
    cout << "\b" << char(255) << "\b" ;
    login.resize(login.size() - 1); // str--
}

else if (login.size() < 10) // ������� ������  && truekey(key
{
login+=a;
cout << a;
}
 
class crpDB extends peDB
{
    public static $countNews = 0;
   
    
    /*
     * Articles
     */
    
    
    public static function getNews($From, $Limit)
    {
        $Result = self::Query("SELECT * FROM %s ORDER BY ID DESC LIMIT %d,%d", 
                crpDB_Articles, $From, $Limit);
        if ($Result)
        {
            $Objects = array();
            while($Object = $Result->fetch_object())
                $Objects[] = $Object;
            self::$countNews = count($Objects);
            return $Objects;
        }
	return array();
    }
    
    public static function addArticle($Params)
    {
	self::Query("INSERT INTO %s %s", crpDB_Articles, self::formInsert($Params));
    }
    
    public static function getArticle($Where)
    {
	$Result = self::Query("SELECT * FROM %s WHERE %s", crpDB_Articles, self::formSelect($Where));
	return $Result->fetch_object();
    }
    
    public static function updArticle($Set, $Where)
    {
	self::Query("UPDATE %s SET %s WHERE %s", crpDB_Articles, self::formUpdate($Set), self::formUpdate($Where));
    }
    
    public static function delArticle($Where)
    {
	self::Query("DELETE FROM %s WHERE %s", crpDB_Articles, self::formUpdate($Where));
    }
    
    
    /*
     * Users
     */
    
    
    public static function getUsers($From, $Limit)
    {
	$Result = self::Query("SELECT * FROM %s ORDER BY ID DESC LIMIT %d,%d", 
                crpDB_Accounts, $From, $Limit);
        if ($Result)
        {
            $Objects = array();
            while($Object = $Result->fetch_object())
                $Objects[] = $Object;
            return $Objects;
        }
	return array();
    }
    
    public static function getUser($Params, $Mod = "AND")
    {
        $String = self::formSelect($Params, $Mod);
        $Result = self::Query("SELECT * FROM %s WHERE %s", crpDB_Accounts, $String)->fetch_assoc();
        if (count($Result) > 0)
        {
            $User = new stdClass();
            foreach($Result as $Key => $Value)
            {
                $Key = ucfirst($Key);
		$User->$Key = $Value;
            }
            return $User;
        }
        return null;
    }
    
    public static function addUser($Response)
    {
        if (self::getUser(array("Login" => $Response->Login, "Email" => $Response->Email), "OR"))
            peCore::throwError(6);
        $Params = array(
            "Login" => crpUtils::Login($Response->Login),
            "Password" => crpUtils::Password($Response->Password),
            "Email" => crpUtils::Email($Response->Email)
        );
        self::Query("INSERT INTO %s %s", crpDB_Accounts, self::formInsert($Params));
    }
    
    public static function updateUser($Set, $Where)
    {
        self::Query("UPDATE %s SET %s WHERE %s", crpDB_Accounts, self::formUpdate($Set), self::formUpdate($Where));
    }
    
    
    /*
     * Characters
     */
    
    
    public static function getPlayers($From, $Limit)
    {
	$Result = self::Query("SELECT %s FROM %s ORDER BY playerID DESC LIMIT %d,%d", 
		crpDB_CharactersFields, crpDB_Characters, $From, $Limit);
        if ($Result)
        {
            $Objects = array();
            while($Object = $Result->fetch_object())
                $Objects[] = crpUtils::translateSelect($Object);
            return $Objects;
        }
	return array();
    }
    
    public static function getCharacters($Params, $Mod = "AND")
    {
	$String = self::formSelect(crpUtils::tranlateParams($Params), $Mod);
	$Result = self::Query("SELECT %s FROM %s WHERE %s", 
                crpDB_CharactersFields, crpDB_Characters, $String);
        if ($Result)
        {
            $Objects = array();
            while($Object = $Result->fetch_object())
                $Objects[] = crpUtils::translateSelect($Object);
            return $Objects;
        }
    }
    
    public static function getCharacter($Where)
    {
	$Where = crpUtils::tranlateParams($Where);
	$Result = self::Query("SELECT %s FROM %s WHERE %s", crpDB_CharactersFields, crpDB_Characters, self::formSelect($Where));
	return crpUtils::translateSelect($Result->fetch_object());
    }
    
    public static function updCharacter($Set, $Where)
    {
	self::Query("UPDATE %s SET %s WHERE %s", crpDB_Characters, 
		self::formUpdate(crpUtils::tranlateParams($Set)), 
		self::formUpdate(crpUtils::tranlateParams($Where))
	);
    }
    
    public static function addCharacter($Params)
    {
        self::Query("INSERT INTO %s %s", crpDB_Characters, self::formInsert(crpUtils::tranlateParams($Params)));
    }
    
    public static function delCharacter($Where)
    {
	$Character = self::getCharacter($Where);
	self::Query("DELETE FROM %s WHERE %s", crpDB_Characters, self::formUpdate(crpUtils::tranlateParams($Where)));
	return $Character;
    }
}

?>
