<?php

/**
 * Description of crpPage
 *
 * @author Vladislav Gritsenko (Inlife)
 */

class peResponse
{
    protected $Data = array();
    
    public function __set($Key, $Value)
    {
	return $this->Data[strtoupper($Key)] = $Value;
    }

    public function __get($Key)
    {
	if (isset($this->Data[strtoupper($Key)]))
	    return $this->Data[strtoupper($Key)];
	return null;
    }
}

?>
