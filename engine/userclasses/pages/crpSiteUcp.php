<?php

/**
 * Description of crpPage
 *
 * @author Vladislav Gritsenko (Inlife)
 */

class crpSiteUcp extends crpSitePage
{
    public function __construct()
    {
        parent::__construct("{(ucp.title)}", "ucp.main");
	$this->Content = new peContent("ucp.main");
	$this->Content->Title = "{(ucp.title)}";
	$this->Content->LinkCharacters = crpUtils::formUrl(array("action" => "characters"), crpPage_SiteUcp);
	$this->Content->LinkStatistics = crpUtils::formUrl(array("action" => "statistics"), crpPage_SiteUcp);
	$this->Content->LinkOptions = crpUtils::formUrl(array("action" => "options"), crpPage_SiteUcp);
	$this->Content->LinkAdditionals = crpUtils::formUrl(array("action" => "additionals"), crpPage_SiteUcp);
    }
}

?>
