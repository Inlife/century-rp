<?php
/*
 *  @Author: Vladislav Gritsenko (Inlife)
 *  @Name: index
 *  @Project: Proto Engine CMS
 */
require("core.php");

$Page = new crpSiteIndex();

@$Code = peCore::getInput($_GET["page"], peInput_Int);
if (!$Code or $Code < 1) $Code = 0;

$News = crpDB::getNews($Code * crpNews_PerPage, crpNews_PerPage);
foreach($News as $i => $Article)
{
    $Blocks[$i] = new peBlock("news.article");
    $Blocks[$i]->Title = $Article->Title;
    $Blocks[$i]->Author = $Article->Author;
    $Blocks[$i]->Content = $Article->Content;
    $Blocks[$i]->Date = $Article->Date;
    $Blocks[$i]->ForumLink = $Article->Forum;
}
$Page->Content->News = new peComponents($Blocks);

if (crpDB::$countNews >= crpNews_PerPage or $Code > 0)
{
    $PageNav = new peBlock("news.navigation");
    $PageNav->Prev = crpUtils::formUrl(array("page" => ($Code - 1)), crpPage_SiteIndex);
    $PageNav->Next = crpUtils::formUrl(array("page" => ($Code + 1)), crpPage_SiteIndex);
    $PageNav->Page = $Code;
    $Page->Content->PageNav = $PageNav;
}


?>