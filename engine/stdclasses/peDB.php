<?php

/**
 * Description of peDB
 *
 * @author Vladislav Gritsenko (Inlife)
 */

abstract class peDB 
{
    protected static $Pointer;
    protected static $LastID;

    public static function Query()
    {
        $Args = func_get_args();
        $pDB = self::getPointer();
        $SQL = array_shift($Args);
        $res = $pDB->query(vsprintf($SQL, $Args));
        self::$LastID = $pDB->insert_id;
        return $res;
    }

    public static function getPointer()
    {
        if (!self::$Pointer)
            return new mysqli(crpDB_Server, crpDB_User, crpDB_Password, crpDB_Name);
        return self::$Pointer;
    }

    public static function getLastID()
    {
        return self::$LastID;
    }
    
        protected static function formSelect($Array, $Mod = "AND")
    {
        foreach($Array as $Key => $Value)
        {
            if(gettype($Value) == "string" OR $Value === null)
                $Value = "'$Value'";
            $Data[] = $Key . "=" . $Value;
        }
        return implode(" $Mod ", $Data);
    }
    
    protected static function formInsert($Params)
    {
        foreach($Params as $Value)
        {
            if(gettype($Value) == "string" OR $Value === null)
                $Value = "'$Value'";
            $Data[] = $Value;
        }
        return "(".implode(",", array_keys($Params)).") VALUES (".implode(",", $Data).")";
    }
    
    protected static function formUpdate($Params)
    {
        foreach($Params as $Key => $Value)
        {
            if(gettype($Value) == "string" OR $Value === null)
                $Value = "'$Value'";
            $Data[] = $Key ."=". $Value;
        }
        return implode(",", $Data);
    }
}

?>
