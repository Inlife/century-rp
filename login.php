<?php

/*
 *  @Author: Vladislav Gritsenko (Inlife)
 *  @Name: login
 *  @Project: Proto Engine CMS
 */
require("core.php");

$Page = new crpSiteLogin();

// logs
peLogProvider::LogCall(peLog_UserLoginFile);

@$Step = peCore::getInput($_GET['action']);
switch($Step)
{
    case null:
        if (crpUser::isLogined()) peCore::throwError(11);
        
        $Response = peCore::getResponse("login", "password");
        $Params = array(
            "Login" => crpUtils::Login($Response->Login),
            "Password" => crpUtils::Password($Response->Password),
            "Activated" => true
        );
	
        $Object = crpDB::getUser($Params);
        if (!$Object->ID) {peCore::throwError(10);}
        
        crpUser::Save($Object);
        
        $Page->Content->Title = "{(login.title)}";
        $Page->Content->Text = "{(login.text)}";
    break;
    case 'logout':
        crpUser::unSave();
        crpSiteIndex::redirectThere();
    break;
}
?>
