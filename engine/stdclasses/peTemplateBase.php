<?php

/**
 * Description of peTemplateBase
 *
 * @author Vladislav Gritsenko (Inlife)
 */

abstract class peTemplateBase 
{
    protected $FileName;
    protected $Data = array();
    protected $Extension = peTpl_Ext;
    
    public function __construct($FileName = null, $Ext = null)
    {
        $this->FileName = $FileName;
        if ($Ext) $this->Extension = $Ext;
    }
    
    public function getContent()
    {
        $Strings = array();
        $Content = peCore::fileGet(peTpl_Dir . $this->FileName . $this->Extension);
        preg_match_all(peTpl_VarMask, $Content, $Strings);
        $Borders = explode("VAR", peTpl_VarSyn);
        foreach($Strings[1] as $Var)
            $Content = str_replace($Borders[0] . $Var . $Borders[1], $this->$Var, $Content);
        return $Content;
    }
    
    public function __set($Key, $Value)
    {
        $this->Data[strtoupper($Key)] = $Value;
        if (is_object($Value) && $Value instanceof peTemplateComponent)
            $this->Data[strtoupper($Key)]->setParent($this);
    }
    
    public function __get($Key)
    {
        if (isset($this->Data[strtoupper($Key)]))
        return $this->Data[strtoupper($Key)];
    }
    
    public function __tostring()
    {
        return $this->getContent();
    }
    
    public function getStyle()
    {
        return $GLOBALS["CurrentPage"]->Style;
    }
}

?>
