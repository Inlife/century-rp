<?php

/*
 *  @Author: Vladislav Gritsenko (Inlife)
 *  @Name: register
 *  @Project: Proto Engine CMS
 */

require('core.php');

$Page = new crpSiteRegister();

// logs
peLogProvider::LogCall(peLog_UserRegisterFile);

if (crpUser::isLogined()) peCore::throwError(11);

@$Step = peCore::getInput($_GET['action'], peInput_Int);

switch($Step)
{
    default:
        $Questions = new crpQuestions(crpData_QuestionsSelect);
        $Questions->Shuffle();
        
        foreach($Questions->get() as $n => $Object)
        {
            $Comp = new peBlock("question.select");
            $Comp->Question = $Object->Question;
            $Answers = array();
            foreach($Object->Answers as $ID => $Answer)
            {
                $aBlock = new peBlock("question.answer");
                $aBlock->Answer = $Answer;
                $aBlock->ID = $ID;
                $aBlock->QuestionHash = md5($Object->Question);
                $Answers[] = $aBlock;
            }
            $Comp->Answers = new peComponents($Answers);
            $Objects[] = $Comp;
            if ($n >= crpData_QuestionsSelectLimit) break;
        }
        $Page->Content->Questions = new peComponents($Objects);
        $Page->Content->FormLink = crpUtils::formUrl(array("action" => 1), crpPage_SiteRegister);
        
    break;
    case 1:
        
        $Questions = new crpQuestions(crpData_QuestionsSelect);
        $Count = 0;
        foreach((array)$_POST as $rHash => $rAnswer)	
        {
            $Hash = peCore::getInput($rHash);
            $Answer = peCore::getInput($rAnswer);
            $Right = $Questions->getAnswerBy($Hash);
            if (!empty($Right))
            {
                if ($Right != $Answer)
                    peCore::throwError(5);
            }
            $Count++;
        }
        if ($Count < 10) peCore::throwError(5);
        crpUser::set("Questions", true);	
        crpSiteRegister::redirectThere(array("action" => 2));
        
    break;
    case 2:
        
	$Page->Scripts = new peJScript("recaptcha");
        $Content = new peContent("register.form");
	$Content->Wiget = recaptcha_get_html(crpSec_reCaptchaPublic);
        $Content->FormUrl = crpUtils::formUrl(array("action" => 3), crpPage_SiteRegister);
        $Page->Content = $Content;
        
    break;
    case 3:
        
        $Response = peCore::getResponse("login", "password", "repassword", "email");
        if (!crpUser::get("Questions")) peCore::throwError(13);
        
	 $reCAPTCHA = recaptcha_check_answer (crpSec_reCaptchaPrivate,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);
	if (!$reCAPTCHA->is_valid) peCore::throwError(23);
	 
        if ($Response->Password != $Response->Repassword) peCore::throwError(3);
        crpDB::addUser($Response);
        
        $Hash = crpUtils::Hash($Response->Login, crpUtils::Password($Response->Password));
        $Link = crpUtils::formUrl(array("action" => 4, "login" => $Response->Login, "hash" => $Hash), crpPage_SiteRegister);
        crpUtils::Mail("email.register.first", $Response->Email, $Response->Login, $Link);

        $Page->Content = new peContent("message.main");
        $Page->Content->Title = "{(register.stage3.title)}";
        $Page->Content->Text = "{(register.stage3.text)}";
        
        crpUser::set("Response", $Response);
        
    break;
    case 4:
        
        @$Login = peCore::getInput($_GET['login']);
        @$Hash = peCore::getInput($_GET['hash']);				
        $Response = crpUser::get("Response");
        if (!$Login or !$Hash) peCore::throwError(0);
        $User = crpDB::getUser(array("Login" => $Login, "Activated" => 0));
        
        if (crpUtils::Hash($User->Login, $User->Password) == $Hash)
        {        
            if ((strtotime($User->Regtime) - time()) > crpUser_RegTimeout) peCore::throwError(9);

            crpDB::updateUser(array("Activated" => true), array("Login" => $User->Login));

            crpUtils::Mail("email.register.second", $User->Email, $User->Login, $Response->Password);

            $Page->Content = new peContent("message.main");
            $Page->Content->Title = "{(register.stage4.title)}";
            $Page->Content->Text = "{(register.stage4.text)}";

            crpUser::del("Response");
        }else
            peCore::throwError(19);
        
    break;
}
?>
