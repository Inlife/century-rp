<?php

/**
 * Description of peTemplatePage
 *
 * @author Vladislav Gritsenko (Inlife)
 */

class peComponents extends peTemplateComponent
{
    protected $Components = array();
    
    public function __construct($Array = null)
    {
        foreach((array)$Array as $Component)
            $this->Add($Component);
    }
    
    public function setParent($Parent)
    {
        foreach((array)$this->Components as $Component)
        { $Component->setParent($Parent); }
    }
    
    public function getContent()
    {
        $Data = "";
        foreach((array)$this->Components as $Component)
            $Data .= $Component->getContent(); 
        return $Data;
    }
    
    public function Add($Component, $Name = null)
    {
        if (is_object($Component) && $Component instanceof peTemplateComponent)
        {
            if ($Name) 
                $this->Components[$Name] = $Component;
            else
                $this->Components[] = $Component;  
        }
    }
    
    public function Get($Name)
    {
        return $this->Components[$Name];
    }
    
    public function Del($Name)
    {
        unset($this->Components[$Name]);
    }
}

?>
