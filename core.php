<?php
/*
 *  @Author: Vladislav Gritsenko (Inlife)
 *  @Name: core
 *  @Project: Proto Engine CMS
 */

if (!defined('__DIR__'))  define('__DIR__', dirname(__FILE__));    
   
function pre($Data)
{
    if (peSys_Status & peSys_Debug)
    {
        print("<pre>");
        print_r($Data);
        print("</pre>");
    }
}

abstract class peCore
{    
    public static $Time;
    public static $Memory;

    public static function Init($Session = true) 
    {
        self::$Time = microtime(true);
	self::$Memory = memory_get_usage();
        require_once('engine/settings.php');
        if (peSys_Status & peSys_Release) error_reporting(0);
        self::loadComp(__DIR__ . ds . peEngine_Dir);
        if ($Session) peViewer::initSession();
        header("Content-type: text/html; charset=" . peIO_OutCharset);
	peAPI::check();
    }
    
    public static function getFileName(&$FileName)
    {
        $FileName = __DIR__ . ds . $FileName;
    }
    
    public static function file($FileName)
    {
        self::getFileName($FileName);
	if (@$Strings = file($FileName))
        {
            foreach($Strings as $Key => $Value)
                $Strings[$Key] = iconv(peIO_InCharset, peIO_OutCharset, $Value);
            return $Strings;
        }elseif(peSys_Status & peSys_Debug)
            pre(sprintf(peIO_FileErrorMsg, $FileName));
        return false;
    }
    
    public static function fileGet($FileName)
    {
        self::getFileName($FileName);
        if (@$Content = file_get_contents($FileName))
            return iconv(peIO_InCharset, peIO_OutCharset, $Content);
        elseif(peSys_Status & peSys_Debug)
            pre(sprintf(peIO_FileErrorMsg, $FileName));
        return false;
    }
    
    public static function fileSet($FileName, $Content, $Flags = 0)
    {
        self::getFileName($FileName);
	if (@!file_put_contents($FileName, $Content, $Flags))
            return false;
    }
    
    static function getInput($Input, $Format = peInput_Str)
    {
        if ($Format & peIO_Trim)
            $Input = trim($Input);
        if ($Format & peIO_NltoBr)
            $Input = nl2br($Input);
        if ($Format & peIO_Spec)
            $Input = htmlspecialchars($Input);
        if ($Format & peIO_Slash)
            $Input = addslashes($Input);
        if ($Format & peIO_Substr)
            $Input = substr($Input, 0, 255);
        if ($Format & peIO_Int)
            $Input = intval($Input);
        if (empty($Input)) return null;
        if ($Format & peIO_Bool)
            $Input = settype($Input, "bool");
        return $Input;
    }
    
    public static function getResponse()
    {
        $Args = func_get_args();
        $Response = new peResponse();
        foreach($Args as $Input)
        {
            $Exp = explode(":", $Input);
            $String = $Exp[0];
            if (isset($Exp[1]))
            {
                if($Exp[1] == "s")
                    $Format = peInput_Str;
                elseif($Exp[1] == "i")
                    $Format = peInput_Int;
                elseif($Exp[1] == "t")
                    $Format = peInput_Text;
                elseif($Exp[1] == "h")
                    $Format = peInput_Html;
                else
                    $Format = peInput_Bool;
            } else  $Format = peInput_Str;
            $Response->$String = self::getInput($_POST[$String], $Format);
        }
        return $Response;
    }

    public static function loadComp($path)  
    {
        $FILES = scandir($path);
        foreach($FILES as $File) 
        {
            if ($File != "." && $File != "..") 
            {
                $FileName = $path . ds . $File;
                if (is_file($FileName) && strpos($File, peSys_IncludeFileExt)) 
                {
                    require_once($FileName);
                }
                elseif (is_dir($FileName)) 
                {
                    self::LoadComp($FileName);
                }        
            }       
        }    
    }  
    
    public static function countTime()
    {
        $Data[] = round((microtime(true) - self::$Time), 4);
	$Data[] = (memory_get_usage() - self::$Memory) / 1000;
	return  vsprintf("<br>Generated in: %s Sec. - Size: %s KB", $Data);
    } 
     
    public static function redirectOld($Url, $Time) 
    {
        print "<meta http-equiv='refresh' content='$Time; URL=$Url'>";
    }
    
    public static function redirect($Url = peSite_Url) 
    {
        if ($Url) { header('Location: ' . $Url); die(); }
    } 
    
    public static function throwError($Code = 0, $Back = 1)
    {
        self::Redirect(peUtils::formUrl(array("code" => $Code, "back" => $Back), crpPage_SiteError));
    }
} 

peCore::Init();

?>
