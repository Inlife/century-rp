<?php

/**
 * Description of crpPage
 *
 * @author Vladislav Gritsenko (Inlife)
 */

class crpSiteIndex extends crpSitePage
{
    public function __construct()
    {
        parent::__construct("{(index.title)}", "index");
        $this->Header = new peBlock("header");
        $this->addJS("index");
    }
}

?>
