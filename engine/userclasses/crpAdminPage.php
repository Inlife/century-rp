<?php

/**
 * Description of crpSitePage
 *
 * @author Vladislav Gritsenko (Inlife)
 */
class crpAdminPage extends pePage 
{
    public function __construct($Title, $Content, $Style = crpStyle_Admin) 
    {
        if (!crpUser::isLogined())
	    peCore::throwError(12);
	$this->requestPermission(crpPerm_Adminpage);
	parent::__construct($Style);
        $this->Title = crpAdmin_TitlePrefix . $Title;
        $this->Content = new peContent($Content);
	$this->Login = crpUser::getData()->Login;
	
	// Logs
	peLogProvider::LogCall(peLog_AdminPanelFile);
	
	// Links
	$this->LinkArticleAll = crpUtils::formUrl(null, crpPage_AdminArticles);
	$this->LinkArticleAdd = crpUtils::formUrl(array("action" => "edit"), crpPage_AdminArticles);
	$this->LinkArticleEdit = crpUtils::formUrl(null, crpPage_AdminArticles);
	
	$this->LinkUserAll = crpUtils::formUrl(null, crpPage_AdminUsers);
	$this->LinkUserAdd = crpUtils::formUrl(array("action" => "edit"), crpPage_AdminUsers);
	$this->LinkUserEdit = crpUtils::formUrl(null, crpPage_AdminUsers);
	
	$this->LinkCharAll = crpUtils::formUrl(null, crpPage_AdminCharacters);
	$this->LinkCharAdd = crpUtils::formUrl(array("action" => "edit"), crpPage_AdminCharacters);
	$this->LinkCharEdit = crpUtils::formUrl(null, crpPage_AdminCharacters);
	$this->LinkCharActive = crpUtils::formUrl(array("action" => "activation"), crpPage_AdminCharacters);
	
	@$Msg = peCore::getInput($_GET["msg"]);
	if ($Msg)
	{
	    $this->Message = new peBlock("message");
	    $this->Message->Text = '{('.$Msg.')}';
	}
    }
    
    public function requestPermission($Rank)
    {
	if (crpUser::getData()->Rank < $Rank)
	    peCore::throwError(18);
    }
}

?>
