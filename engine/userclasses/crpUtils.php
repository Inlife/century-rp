<?php

/**
 * Description of crpUtils
 *
 * @author Vladislav Gritsenko (Inlife)
 */
class crpUtils extends peUtils
{
    public static function Login($Login)
    {
        if (!$Login) peCore::throwError(1);
        if ((strlen($Login) > crpUser_LenghMax) or (strlen($Login) < crpUser_LenghMin)) 
            peCore::throwError(7);
        if (!preg_match("/^[a-zA-Z0-9_]+$/", $Login)) peCore::throwError(2);
        return $Login;
    }

    public static function Name($String)
    {
        $Login = ucfirst(strtolower($String));
        if (!$String) peCore::throwError(1);
        if ((strlen($String) > crpUser_LenghMax) or (strlen($String) < crpUser_LenghMinName)) 
            peCore::throwError(15);
        if (!preg_match("/^[a-zA-Z]+$/", $String)) peCore::throwError(14);
        return $Login;
    }

    public static function Password($Password, $md5 = true)
    {
        if (!$Password) peCore::throwError(1);
        if ((strlen($Password) > crpUser_LenghMax) or (strlen($Password) < crpUser_LenghMin)) 
            peCore::throwError(8);
        if ($md5) $Password = md5($Password);
        return $Password;
    }

    public static function Email($Email)
    {
        if (!$Email) peCore::throwError(1);
        if (!preg_match('|([a-z0-9_\.\-]{1,20})@([a-z0-9\.\-]{1,20})\.([a-z]{2,6})|is', $Email)) 
                peCore::throwError(4);
        return $Email;
    }
    
    public static function Mail()
    {
        $Args = func_get_args();
        $FileName = array_shift($Args);
        $Email = array_shift($Args);
        self::MailTo($Email, vsprintf(peCore::fileGet(peData_Dir . $FileName . peData_EmailExt), $Args));
	peLogProvider::EmailLogCall($Email, $FileName);
    }
    
    public static function getMaxCharactersBy($Rank)
    {
	return @constant("crpUser_CharactersRank".$Rank);
    }
    
    public static function translateSelect($Character)
    {
	$Response = new peResponse;
	$Response->Sex = $Character->playerGender; unset($Character->playerGender);
	$Response->Date = $Character->playerRegistrationDate; unset($Character->playerRegistrationDate);
	foreach($Character as $Property => $Value)
	{
	    $propName = str_replace("player", "", $Property);
	    $Response->$propName = $Value;
	}
	return $Response;
    }
    
    public static function tranlateParams($preParams)
    {
	if (!empty($preParams["Sex"]))
	{
	    $preParams["Gender"] = $preParams["Sex"]; 
	    unset($preParams["Sex"]);
	}
	$Params = array();
	foreach($preParams as $Key => $Value)
	    $Params["player".$Key] = $Value;
	return $Params;
    }
}

?>
