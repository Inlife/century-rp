<?php

/**
 * Description of crpUser
 *
 * @author Vladislav Gritsenko (Inlife)
 */

class crpUser extends peViewer 
{
    public static function isLogined()
    {
        if (self::getData()) 
            return true;
        return false;
    }
    
    public static function getData()
    {
        if ($Data = self::get("UserData"))
            return $Data;
        else
        {   
	    
            @$Login = peCore::getInput($_COOKIE['clogin']);
            @$Token = peCore::getInput($_COOKIE['ctoken']);	
            if ($Login && $Token)
            {
                $User = crpDB::getUser(array("Login" => $Login));
                if (crpUtils::Hash($User->Password, $User->Login, $User->Email) == $Token)
                {
                    self::set("UserData", $User);
		    
		    // Logs
		    peLogProvider::LogCall(peLog_UserLoginFile);
		    
                    return $User;
                }
            }
            return false;
        }    
    }
    
    public static function Save($Object)
    {
        self::set("UserData", $Object);
        setcookie("clogin", $Object->Login, peCookie_Expire);
        setcookie("ctoken", crpUtils::Hash($Object->Password, $Object->Login, $Object->Email), peCookie_Expire);
    }
    
    public static function unSave()
    {
        self::set("UserData", null);
        setcookie("clogin", null, peCookie_Expire);
        setcookie("ctoken", null, peCookie_Expire);
    }
}

?>
