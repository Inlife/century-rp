<?php

/**
 * Description of crpQuestions
 *
 * @author Vladislav Gritsenko (Inlife)
 */
class crpQuestions 
{
    public $Questions;
    
    public function __construct($FileName)
    {
        $Data = peCore::fileGet(peData_Dir . $FileName);
        $Result = array();
        $exp1 = explode("#", trim($Data));
        foreach($exp1 as $i => $qBlock)
        {
            $Result[$i] = new stdClass();
            if ($qBlock) 
            {
                $exp2 = explode(NL, trim($qBlock));
                foreach($exp2 as $qStr)
                {
                    if ($qStr)
                    {
                        $exp3 = explode("=", trim($qStr));
                        if ($exp3[0] == "question" or $exp3[0] == "answer")
			{
                            $name = ucfirst($exp3[0]);
                            $Result[$i]->$name = trim($exp3[1]);
			}
                        else
			{
                            $Result[$i]->Answers[trim($exp3[0])] = trim($exp3[1]);
			}
                    }
                }
            }
        }
        $this->Questions = $Result;
    }
    
    public function Shuffle()
    {
        shuffle($this->Questions);
    }
    
    public function get()
    {
        return $this->Questions;
    }
    
    public function getAnswerBy($Hash)
    {
        foreach($this->get() as $Object)
        {
            if (md5($Object->Question) == $Hash)
                return $Object->Answer;
        }
        return null;
    }
}

?>
